import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
} from '@ngrx/store';
import { cloneDeep } from 'lodash';
import { TableActions, TableActionTypes } from '../actions/table.actions';

export interface TableState {
  filter?: any;
  order?: any;
  table?: any;
}

export const initialState = {
};

export const tableInitialState: TableState = {
  filter: {},
  order: []
}

export function tableReducer(state = initialState, action: TableActions): { [id: string]: TableState; } {
  switch (action.type) {

    case TableActionTypes.LoadFilter:
      return state[action.id];
    case TableActionTypes.AppendFilter:
        state[action.id] = {
          ...state[action.id],
          filter: cloneDeep(action.filter)
        };
        return state;
   
    case TableActionTypes.AppendState:
          state[action.id] = {
            ...state[action.id],
            table: action.data
          };
          return state;
    default:
      return state;
  }
}

export interface NgxTablesState {
  tables: {
    [id: string]: TableState;
  };
}

export const tableReducers: ActionReducerMap<NgxTablesState> = {
  tables: tableReducer,
};

export const featureName = 'tables';

export const getTablesState = createFeatureSelector(featureName);

export const selectTable = createSelector(
  getTablesState,
  (state: NgxTablesState, props: { id: string }) => {
    return state.tables[props.id];
  }
);