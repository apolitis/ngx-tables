import { Pipe, PipeTransform } from "@angular/core";

/**
 * 
 * @class AdvancedAggregatorPipe
 * @classdesc Given a list of items runs a user defined function against that list
 * 
 */

@Pipe({
  name: 'callAggregator'
})
export class AdvancedAggregatorPipe implements PipeTransform {

  /**
   * 
   * @override
   * 
   * @param items The list of items to run the aggregator against
   * @param functionPool The functions that are available from the list caller
   * @param functionName The name of the function to be called
   * 
   * @returns {string} The value to show at the UI
   * 
   */
  transform(items: any[], functionPool: any, functionName: string): any {
    if (Array.isArray(items) && functionName && functionName in functionPool) {
      return functionPool[functionName](items);
    } else {
      return '-';
    }
  }
}
